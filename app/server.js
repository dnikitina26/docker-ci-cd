
import express from "express";
import { client } from "./redis.js";

client.connect();

const app = express();
app.use(express.json());

const PORT = process.env.PORT || 2003;
app.listen(PORT, () => {
 console.log(`Server is running on port ${PORT}`);
});

app.get('/', (req, res) => {
    res.send('Have a good day!');
});


app.post('/', (req, res) => {
    const { user } = req.body;

    res.send({"result": find(+user)});
})

//До модернизации
//let user = process.argv.slice(2); 
//let mas = Array.from({length:100}, () => Math.floor(Math.random() * 300) ).sort((a,b) => a - b);


let mas;

if(await client.get("mas")) {
    mas = await client.get("mas");
    mas = JSON.parse(mas);
} else {
    mas = Array.from({length:100}, () => Math.floor(Math.random() * 300) ).sort((a,b) => a - b);
    client.set("mas", JSON.stringify(mas));
}


console.log(mas);

console.log(find(user)); 

//До модернизации
//process.exit();

function find(a) {
    let left = 0;
    let right = mas.length - 1;
    let index = -1;

    while(left <= right) {
        let mid = Math.floor((left + right)/2);

        if(mas[mid] === +a) {
            index = mid;
            break;
        }
        else if (mas[mid] < +a) {
            left = mid + 1;
        }
        else {
            right = mid - 1;
        }
    }
    
    if (index === -1) {
        return 'Не найдено';
    }

    return `Индекс в массиве: ${index}`;
}
